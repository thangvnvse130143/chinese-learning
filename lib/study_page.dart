import 'package:chinese_learning/question_page.dart';
import 'package:flutter/material.dart';
import 'package:flutter_staggered_animations/flutter_staggered_animations.dart';

class StudyPage extends StatelessWidget {
  final List<Subject> subjects = [
    Subject(name: 'Ngày tháng'),
    Subject(name: 'Đồ ăn'),
    Subject(name: 'Số đếm'),
    Subject(name: 'Cảm xúc'),
    Subject(name: 'Phỏng vấn'),
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        bottomNavigationBar: BottomAppBar(),
        backgroundColor: Theme.of(context).accentColor,
        body: Padding(
          padding: const EdgeInsets.fromLTRB(32, 32, 32, 0),
          child: AnimationLimiter(
            child: ListView.builder(
              itemCount: subjects.length,
              itemBuilder: (BuildContext context, int index) {
                return AnimationConfiguration.staggeredList(
                  position: index,
                  duration: const Duration(milliseconds: 375),
                  child: SlideAnimation(
                    verticalOffset: 100.0,
                    child: FadeInAnimation(
                      child: Padding(
                        padding: const EdgeInsets.symmetric(vertical: 16.0),
                        child: RaisedButton(
                          padding: EdgeInsets.symmetric(vertical: 32),
                          onPressed: () => Navigator.of(context).push(
                              MaterialPageRoute(
                                  builder: (_) => QuestionPage())),
                          color: Colors.white,
                          textColor: Colors.black,
                          child: Text(subjects[index].name,
                              style: Theme.of(context)
                                  .textTheme
                                  .headline4
                                  .apply(color: Colors.black)),
                        ),
                      ),
                    ),
                  ),
                );
              },
            ),
          ),
        ));
  }
}

class Subject {
  String name;
  Subject({@required this.name});
}
