import 'package:flutter/cupertino.dart';

class Comment {
  String name, content;
  DateTime dateTime;

  Comment({
    @required this.content,
    @required this.dateTime,
    @required this.name,
  });
}
