import 'dart:async';
import 'dart:io';

import 'package:chinese_learning/review_page.dart';
import 'package:circular_check_box/circular_check_box.dart';
import 'package:flutter/material.dart';
import 'package:simple_animations/simple_animations.dart';

class QuestionPage extends StatefulWidget {
  @override
  _QuestionPageState createState() => _QuestionPageState();
}

class _QuestionPageState extends State<QuestionPage> {
  bool _isCorrect;
  bool _isListening;

  @override
  void initState() {
    _isCorrect = false;
    _isListening = false;

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: true,
        title: Image.asset(
          'images/logo.png',
          height: 50,
        ),
      ),
      body: Padding(
        padding: const EdgeInsets.symmetric(vertical: 32, horizontal: 16),
        child: Column(
          children: [
            _buildQuestionRow(context),
            AnimatedCrossFade(
              crossFadeState: _isListening
                  ? CrossFadeState.showSecond
                  : CrossFadeState.showFirst,
              duration: Duration(seconds: 1),
              firstChild: Container(
                child: _buildCheckBox(),
                width: double.infinity,
              ),
              secondChild: Padding(
                padding: const EdgeInsets.all(50),
                child: _buildMic(context),
              ),
            ),
            Align(
              alignment: Alignment.bottomCenter,
              child: GridView.count(
                crossAxisCount: 2,
                childAspectRatio: 3,
                children: [
                  AnswerBox(answer: 'a', onAnswered: _onAnswered),
                  AnswerBox(answer: 'b', onAnswered: _onAnswered),
                  AnswerBox(answer: 'c', onAnswered: _onAnswered),
                  AnswerBox(answer: 'd', onAnswered: _onAnswered),
                ],
                crossAxisSpacing: 16,
                mainAxisSpacing: 16,
                shrinkWrap: true,
              ),
            )
          ],
        ),
      ),
    );
  }

  Widget _buildMic(BuildContext context) {
    return MirrorAnimation<Color>(
      tween: ColorTween(
        begin: Theme.of(context).primaryColor,
        end: Colors.blueAccent,
      ),
      builder: (context, child, value) => Container(
        height: 300,
        width: 300,
        decoration: BoxDecoration(
          shape: BoxShape.circle,
          border: Border.all(
            color: value,
            width: 4,
          ),
        ),
        child: child,
      ),
      child: FlatButton(
        onPressed: () => Navigator.of(context).push(MaterialPageRoute(
          builder: (context) => ReviewPage(),
        )),
        child: MirrorAnimation(
          tween: Tween<double>(begin: 90, end: 110),
          builder: (context, child, value) => Icon(
            Icons.mic,
            size: value,
            color: Theme.of(context).primaryColor,
          ),
        ),
      ),
    );
  }

  Widget _buildCheckBox() {
    return Container(
      height: 400,
      child: Transform.scale(
        scale: 400 / Checkbox.width / 2,
        child: CircularCheckBox(
            value: _isCorrect,
            inactiveColor: Colors.transparent,
            onChanged: (_) {
              setState(() {
                _isListening = true;
              });
            }),
      ),
    );
  }

  void _onAnswered(answer) {
    setState(() {
      _isCorrect = !_isCorrect;
    });
  }

  Widget _buildQuestionRow(BuildContext context) {
    return Row(
      children: [
        Icon(
          Icons.volume_up,
          size: 64,
          color: Theme.of(context).accentColor,
        ),
        QuestionBox(question: _isCorrect ? 'a' : ' '),
        QuestionBox(question: 'j')
      ],
      mainAxisAlignment: MainAxisAlignment.spaceAround,
    );
  }
}

class AnswerBox extends StatelessWidget {
  const AnswerBox({Key key, @required this.answer, @required this.onAnswered})
      : super(key: key);
  final String answer;
  final Function(String) onAnswered;

  @override
  Widget build(BuildContext context) {
    var textStyle = Theme.of(context)
        .textTheme
        .headline5
        .copyWith(fontWeight: FontWeight.bold, color: Colors.white);
    return Container(
      decoration: BoxDecoration(
          color: Theme.of(context).accentColor,
          borderRadius: BorderRadius.circular(16)),
      child: FlatButton(
        onPressed: () => onAnswered(answer),
        child: Center(
          child: Text(
            answer,
            style: textStyle,
            textAlign: TextAlign.center,
          ),
        ),
      ),
      // width: 120,
      padding: EdgeInsets.symmetric(vertical: 8),
    );
  }
}

class QuestionBox extends StatelessWidget {
  const QuestionBox({Key key, @required this.question}) : super(key: key);
  final String question;

  @override
  Widget build(BuildContext context) {
    var textStyle = Theme.of(context)
        .textTheme
        .headline5
        .copyWith(fontWeight: FontWeight.bold, color: Colors.white);
    return Container(
      decoration: BoxDecoration(
          color: Theme.of(context).accentColor,
          borderRadius: BorderRadius.circular(16)),
      child: AnimatedSwitcher(
        duration: Duration(milliseconds: 500),
        child: Text(
          question,
          key: ValueKey<String>(question),
          style: textStyle,
          textAlign: TextAlign.center,
        ),
      ),
      width: 100,
      padding: EdgeInsets.symmetric(vertical: 8),
    );
  }
}
